function Selector2( data = {
    id:'default',
    url:undefined,
    data:undefined
} ){
    var select = document.getElementById( data.id );
    select.data = this;
    
    //CONSTRUCTOR
    (function(){
        select.innerHTML = '';
        if( data.url ){
            fill();
        }
    })();
    
    function fill(){
        select.innerHTML = '';
        getData().then(function (respose) {
            console.log(respose);
            let render = data.renderData || function (d) {
                return d;
            };
            respose = render( respose );
            respose.forEach(function ( item ) {
                let opcion = new Option( renderValue( item ), item[data.tagId] );
                opcion.data = item;
                select.appendChild(opcion);
            });
        }).catch(function (error) {
            console.log(error);
        });
        
    }
    
    function renderValue( item ){
        if( typeof ( data.tagValor ) === 'string' ){
            return item[data.tagValor];
        }else if( typeof ( data.tagValor ) === 'function' ){
            return data.tagValor( item );
        }
    }
        
    function getData(){
        return new Promise(function(resolve,reject){
            var xhttp;
            if (window.XMLHttpRequest){xhttp = new XMLHttpRequest();}
            else{xhttp = new ActiveXObject("Microsoft.XMLHTTP");}
            
            xhttp.onreadystatechange = function(){
                
            };
            xhttp.onload = function () {
                if (this.readyState === 4 && this.status === 200) {//EXITO
                    resolve(this.response);
                }else if (this.readyState === 4 && this.status === 404) {///NO ENCONTRADO
                    reject(this);
                }else if (this.readyState === 4){
                    reject(this);
                }
            };
            xhttp.onerror = function(error){
                reject("Fallo la conexión.");
            };
            xhttp.open('POST', data.url, false);
            let parametros = data.parametros || {id:1};
            xhttp.send(JSON.stringify(parametros)); 
        });
    }
    
    this.reset = function(){
        fill();
    };
    
}