XHR.config = {
    metodo: 'post',///NO SE USA SI SE UTILIZA EL UNA FUNCION ESPECIFICA
    url:'',
    parametros:'',
    separador:'?',///SOLO PARA PATECIONES REST
    Tipo:'json',///SI LA RESPUESTA SE QUIERE PARSEAR A JSON
    form: undefined,///OBJETO FORM
    formData:true,//SI SE VA A MANDAR POR MULTIPART,
    soloFile:true// SI SOLO QUIERO QUE SE METAN LOS INPUT FILE
};
XHR.GET = 'GET';
XHR.POST = 'POST';
XHR.PUT = 'PUT';
XHR.DELETE = 'DELETE';
XHR.OPTIONS = 'OPTIONS';
XHR.multiplesXHR = function(...objetc){
    let xhrs = [];
    objetc.forEach(function(obj){
        xhrs.push(new XHR(obj));
    });
    return Promise.all(xhrs);
};
function XHR(xhr){
    function parseRequest(peticion,data){
        let response = peticion.response;
        if (data.Tipo === 'json')
            response = JSON.parse(response);
        return response;
    }
    function run(data) {
        return new Promise(function(resolve,reject){
            var xhttp;
            if (window.XMLHttpRequest){xhttp = new XMLHttpRequest();}
            else{xhttp = new ActiveXObject("Microsoft.XMLHTTP");}
            
            xhttp.onreadystatechange = function(){
                
            };
            xhttp.onload = function () {
                if (this.readyState === 4 && this.status === 200) {//EXITO
                    resolve(parseRequest(this,data),this);
                }else if (this.readyState === 4 && this.status === 404) {///NO ENCONTRADO
                    reject(this);
                }else if (this.readyState === 4){
                    reject(this);
                }
            };
            if(data.tipo === XHR.GET){
                xhttp.open(data.tipo, data.url, data.asincrono);
                xhttp.send(); 
            }
            else if(data.tipo === XHR.POST || data.tipo === XHR.PUT || data.tipo === XHR.DELETE){
                xhttp.open(data.tipo, data.url, data.asincrono);
                if(data.Content_type !== undefined)
                    xhttp.setRequestHeader(data.Content_type.Content_type, data.Content_type.type);
                xhttp.send(data.parametros); 
            }
        });
    };
    
    function parseStringParameters(data){
        let parameters = "";
        for (let clave in data.parametros){
            parameters += clave+"="+data.parametros[clave]+"&";
        }
        data.parametros = parameters.substring(0,parameters.length-1);
    }
    function parseStringToFormData(formData,cadena){
        if(cadena === undefined)
            return;
        
        cadena.split("&").forEach(function(item){
            let para = item.split("="); 
            formData.append(para[0],para[1]);
        });
        
    }
    function parseFormToString(data){
        let parameters = '';
        for(let i=0;i<data.form.elements.length;i++){
            if(data.form.elements[i].type === 'submit'){
                continue;
            }
            if(data.form.elements[i].type === 'checkbox' && !data.form.elements[i].checked){
                continue;
            }
            if(data.form.elements[i].type === 'radio' && !data.form.elements[i].checked){
                continue;
            }
            parameters += data.form.elements[i].name+"="+data.form.elements[i].value+"&";
        }
        parameters = parameters.substring(0,parameters.length-1);
        if(data.parametros === undefined || data.parametros === ''){
            data.parametros = parameters;
        }else{
            data.parametros += "&"+parameters;
        }
    }
    function prepare(data){
        data.Content_type = {
            Content_type:'Content-type',
            type:'application/x-www-form-urlencoded'
        };
        data.asincrono = data.asincrono || true;
        if(typeof (data.parametros) === 'object'){
            parseStringParameters(data);
        }
        ////PARAMETRIZAR CON FORM
        if(data.form !== undefined && data.formData){
            let oData = new FormData(data.form);
            parseStringToFormData(oData,data.parametros);
            data.parametros = oData;
            data.Content_type = undefined;
        }else if(data.form !== undefined){
            parseFormToString(data);
        }
    }
    
    this.get = function(data = XHR.config){
        data.tipo = XHR.GET;
        data.formData = false;
        prepare(data);
        data.url += (data.separador || "?")+data.parametros; 
        return run(data);
    };
    this.post = function(data = XHR.config){
        data.tipo = XHR.POST;
        data.formData = data.formData || false;
        prepare(data);
        return run(data);
    };
    this.put = function(data = XHR.config){
        data.tipo = XHR.PUT;
        data.formData = data.formData || false;
        prepare(data);
        return run(data);
    };
    this.delete = function(data = XHR.config){
        data.tipo = XHR.DELETE;
        data.formData = data.formData || false;
        prepare(data);
        return run(data);
    };
    
    ///REALIZAR PETICION
    if(xhr !== undefined){
        xhr.metodo = xhr.metodo || XHR.POST;
        xhr.metodo = xhr.metodo.toUpperCase();
        if(xhr.metodo === XHR.GET)
            return this.get(xhr);
        else if(xhr.metodo === XHR.POST)
            return this.post(xhr);
        else if(xhr.metodo === XHR.PUT)
            return this.put(xhr);
        else if(xhr.metodo === XHR.DELETE)
            return this.delete(xhr);
        else
            return this.post(xhr);
    }
}