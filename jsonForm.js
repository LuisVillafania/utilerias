
function jsonForm(id,data){
    var form;
    ///CONSTRUCTOR
    (()=>{
        form = document.getElementById(id);
        active();
    })();

    

    function active(){
        for(let i=0;i<form.length;i++){
            let elemento = form[i];
            ///funcion anonima
            (function(elemet){
                if(
                    elemet.type === 'text'     || elemet.type === 'date'     || elemet.type === 'email' ||
                    elemet.type === 'password' || elemet.type === 'color'    || elemet.type === 'tel'   ||
                    elemet.type === 'time'     || elemet.type === 'textarea'
                ){
                    elemet.oninput = function(){
                        data[elemet.name] = this.value || "";
                    }
                    elemet.oninput();
                }else if( 
                    elemet.type === 'number' || elemet.type === 'range' || elemet.type === 'select-one' ||
                    elemet.type === 'radio'
                ){
                    elemet.oninput = function(){
                        data[elemet.name] = parseInt(this.value) || -1;
                    }
                    elemet.oninput();
                }else if(  elemet.type === 'checkbox' ){
                    elemet.oninput = function(){
                        data[elemet.name] = ((this.checked)?(parseInt(this.value) || 0):0);
                    }
                    elemet.oninput();
                }else if(  elemet.type === 'file' ){
                    elemet.value = '';
                    elemet.onchange = function(evt){
                        let file = evt.target.files[0];
                        
                        var reader = new FileReader();
                        reader.onload = function(e) {

                            data[elemet.name] = {
                                lastModified: file.lastModified,
                                name:         file.name,
                                size:         file.size,
                                type:         file.type,
                                file:         e.target.result
                            };
                        };
                        reader.readAsDataURL(file);
                        
                    };
                }else{
                    console.log(elemet.type);
                }
            })(elemento);
        }
    }///FIN DE ACTIVE

    this.getDataObjeto = function(){
        return data;
    }
}

export {jsonForm}